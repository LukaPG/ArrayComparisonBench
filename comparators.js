/**
 * 
 * @param {any[]} one 
 * @param {any[]} two 
 * @returns {boolean}
 */
function compareInLoop(one, two) {
    for (let i = 0; i < one.length; i++) {
        if (one[i] !== two[i]) return false;
    }
    return true;
}

/**
 *
 * @param {any[]} one
 * @param {any[]} two
 * @returns {boolean}
 */
function compareWithJSON(one, two) {
    return JSON.stringify(one) === JSON.stringify(two);
}

module.exports = {
    compareInLoop: compareInLoop,
    compareWithJSON: compareWithJSON
}