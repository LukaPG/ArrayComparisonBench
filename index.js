const { compareInLoop, compareWithJSON } = require("./comparators");
const { Suite } = require("benchmark");

const arrayOfPrimitives = [1, 2, 3, 4, "asdf", 343.23, true, 43, 0];
const secondArrOfPrimitives = arrayOfPrimitives.map(element => element);

const arrOfComplex = [{ asdf: 2 }, 1, new Date(), [1, 2, 3]];
const secondComplexArr = [{ asdf: 2 }, 1, new Date(), [2, 3]];

const suite = new Suite("Loop comparison benchmark");

suite
    .add("looper over same arrays of primitives", () => {
        compareInLoop(arrayOfPrimitives, secondArrOfPrimitives);
    })
    .add("stringify same arrays of primitives", () => {
        compareWithJSON(arrayOfPrimitives, secondArrOfPrimitives);
    })
    .add("looper over diff arrays of complex objects", () => {
        compareInLoop(arrOfComplex, secondComplexArr);
    })
    .add("stringify diff arrays of complex objects", () => {
        compareWithJSON(arrOfComplex, secondComplexArr);
    })
    // add listeners
    .on('cycle', function (event) {
        console.log(String(event.target), event);
    })
    .on('complete', function () {
        console.log('Fastest is ' + this.map('name'));
    })
    .run({ async: true });

